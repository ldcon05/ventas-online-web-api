﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebApiVentasOnline.Models;

namespace WebApiVentasOnline.Controllers
{
    public class CarritoController : ApiController
    {
        private WebApiVentasOnlineServiceContext db = new WebApiVentasOnlineServiceContext();

        // GET api/Carrito
        public IQueryable<Carrito> GetCarritoes()
        {
            return db.Carritoes
                .Include(u => u.Usuario);
        }

        // GET api/Carrito/5
        [ResponseType(typeof(Carrito))]
        public async Task<IHttpActionResult> GetCarrito(int id)
        {
            Carrito carrito = await db.Carritoes.FindAsync(id);
            if (carrito == null)
            {
                return NotFound();
            }

            return Ok(carrito);
        }

        // PUT api/Carrito/5
        public async Task<IHttpActionResult> PutCarrito(int id, Carrito carrito)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != carrito.IdCarrito)
            {
                return BadRequest();
            }

            db.Entry(carrito).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CarritoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Carrito
        [ResponseType(typeof(Carrito))]
        public async Task<IHttpActionResult> PostCarrito(Carrito carrito)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Carritoes.Add(carrito);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = carrito.IdCarrito }, carrito);
        }

        // DELETE api/Carrito/5
        [ResponseType(typeof(Carrito))]
        public async Task<IHttpActionResult> DeleteCarrito(int id)
        {
            Carrito carrito = await db.Carritoes.FindAsync(id);
            if (carrito == null)
            {
                return NotFound();
            }

            db.Carritoes.Remove(carrito);
            await db.SaveChangesAsync();

            return Ok(carrito);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CarritoExists(int id)
        {
            return db.Carritoes.Count(e => e.IdCarrito == id) > 0;
        }
    }
}