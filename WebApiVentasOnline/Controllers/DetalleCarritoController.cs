﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebApiVentasOnline.Models;

namespace WebApiVentasOnline.Controllers
{
    public class DetalleCarritoController : ApiController
    {
        private WebApiVentasOnlineServiceContext db = new WebApiVentasOnlineServiceContext();

        // GET api/DetalleCarrito
        public IQueryable<DetalleCarrito> GetDetalleCarritoes()
        {
            return db.DetalleCarritoes
                .Include(c => c.Carrito)
                .Include(p => p.Producto)
                ;
        }

        // GET api/DetalleCarrito/5
        [ResponseType(typeof(DetalleCarrito))]
        public async Task<IHttpActionResult> GetDetalleCarrito(int id)
        {
            DetalleCarrito detallecarrito = await db.DetalleCarritoes.FindAsync(id);
            if (detallecarrito == null)
            {
                return NotFound();
            }

            return Ok(detallecarrito);
        }

        // PUT api/DetalleCarrito/5
        public async Task<IHttpActionResult> PutDetalleCarrito(int id, DetalleCarrito detallecarrito)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != detallecarrito.IdDetalle)
            {
                return BadRequest();
            }

            db.Entry(detallecarrito).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DetalleCarritoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/DetalleCarrito
        [ResponseType(typeof(DetalleCarrito))]
        public async Task<IHttpActionResult> PostDetalleCarrito(DetalleCarrito detallecarrito)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DetalleCarritoes.Add(detallecarrito);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = detallecarrito.IdDetalle }, detallecarrito);
        }

        // DELETE api/DetalleCarrito/5
        [ResponseType(typeof(DetalleCarrito))]
        public async Task<IHttpActionResult> DeleteDetalleCarrito(int id)
        {
            DetalleCarrito detallecarrito = await db.DetalleCarritoes.FindAsync(id);
            if (detallecarrito == null)
            {
                return NotFound();
            }

            db.DetalleCarritoes.Remove(detallecarrito);
            await db.SaveChangesAsync();

            return Ok(detallecarrito);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DetalleCarritoExists(int id)
        {
            return db.DetalleCarritoes.Count(e => e.IdDetalle == id) > 0;
        }
    }
}