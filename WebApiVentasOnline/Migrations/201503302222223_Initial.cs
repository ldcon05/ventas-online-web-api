namespace WebApiVentasOnline.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Carritoes",
                c => new
                    {
                        IdCarrito = c.Int(nullable: false, identity: true),
                        Fecha = c.String(nullable: false),
                        UsuarioId = c.Int(nullable: false),
                        Usuario_IdUsuario = c.Int(),
                    })
                .PrimaryKey(t => t.IdCarrito)
                .ForeignKey("dbo.Usuarios", t => t.Usuario_IdUsuario)
                .Index(t => t.Usuario_IdUsuario);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        IdUsuario = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        Apellido = c.String(nullable: false),
                        Nickname = c.String(nullable: false),
                        Pass = c.String(nullable: false),
                        Telefono = c.Int(nullable: false),
                        Direccion = c.String(nullable: false),
                        Correo = c.String(),
                        RolId = c.Int(nullable: false),
                        Rol_IdRol = c.Int(),
                    })
                .PrimaryKey(t => t.IdUsuario)
                .ForeignKey("dbo.Rols", t => t.Rol_IdRol)
                .Index(t => t.Rol_IdRol);
            
            CreateTable(
                "dbo.Rols",
                c => new
                    {
                        IdRol = c.Int(nullable: false, identity: true),
                        NombreRol = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdRol);
            
            CreateTable(
                "dbo.DetalleCarritoes",
                c => new
                    {
                        IdDetalle = c.Int(nullable: false, identity: true),
                        Cantidad = c.Int(nullable: false),
                        CarritoId = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                        Carrito_IdCarrito = c.Int(),
                        Producto_IdProducto = c.Int(),
                    })
                .PrimaryKey(t => t.IdDetalle)
                .ForeignKey("dbo.Carritoes", t => t.Carrito_IdCarrito)
                .ForeignKey("dbo.Productoes", t => t.Producto_IdProducto)
                .Index(t => t.Carrito_IdCarrito)
                .Index(t => t.Producto_IdProducto);
            
            CreateTable(
                "dbo.Productoes",
                c => new
                    {
                        IdProducto = c.Int(nullable: false, identity: true),
                        NombreProducto = c.String(nullable: false),
                        Descripcion = c.String(nullable: false),
                        Precio = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Foto = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdProducto);
            
            CreateTable(
                "dbo.Facturas",
                c => new
                    {
                        IdFactura = c.Int(nullable: false, identity: true),
                        CarritoId = c.Int(nullable: false),
                        Carrito_IdCarrito = c.Int(),
                    })
                .PrimaryKey(t => t.IdFactura)
                .ForeignKey("dbo.Carritoes", t => t.Carrito_IdCarrito)
                .Index(t => t.Carrito_IdCarrito);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Facturas", "Carrito_IdCarrito", "dbo.Carritoes");
            DropForeignKey("dbo.DetalleCarritoes", "Producto_IdProducto", "dbo.Productoes");
            DropForeignKey("dbo.DetalleCarritoes", "Carrito_IdCarrito", "dbo.Carritoes");
            DropForeignKey("dbo.Carritoes", "Usuario_IdUsuario", "dbo.Usuarios");
            DropForeignKey("dbo.Usuarios", "Rol_IdRol", "dbo.Rols");
            DropIndex("dbo.Facturas", new[] { "Carrito_IdCarrito" });
            DropIndex("dbo.DetalleCarritoes", new[] { "Producto_IdProducto" });
            DropIndex("dbo.DetalleCarritoes", new[] { "Carrito_IdCarrito" });
            DropIndex("dbo.Carritoes", new[] { "Usuario_IdUsuario" });
            DropIndex("dbo.Usuarios", new[] { "Rol_IdRol" });
            DropTable("dbo.Facturas");
            DropTable("dbo.Productoes");
            DropTable("dbo.DetalleCarritoes");
            DropTable("dbo.Rols");
            DropTable("dbo.Usuarios");
            DropTable("dbo.Carritoes");
        }
    }
}
