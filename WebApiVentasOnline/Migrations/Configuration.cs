namespace WebApiVentasOnline.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebApiVentasOnline.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<WebApiVentasOnline.Models.WebApiVentasOnlineServiceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WebApiVentasOnline.Models.WebApiVentasOnlineServiceContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Rols.AddOrUpdate(x => x.IdRol,
       new Rol() { IdRol = 1, NombreRol = "admin" },
       new Rol() { IdRol = 2, NombreRol = "cliente" }
       );

            context.Usuarios.AddOrUpdate(x => x.IdUsuario,
       new Usuario() { IdUsuario = 1, Nombre = "Luis", Apellido = "Con", Nickname = "admin", Pass = "123", Correo = "ldcon05@gmial.com", Direccion = "Guatemala", Telefono = 12345678, RolId = 1 },
       new Usuario() { IdUsuario = 2, Nombre = "Daniel", Apellido = "Rustrian", Nickname = "cliente", Pass = "321", Correo = "ldcon@outlook.es", Direccion = "San Jose Pinula", Telefono = 87654321, RolId = 2 }
       );

            context.Carritoes.AddOrUpdate(x => x.IdCarrito,
                 new Carrito() { IdCarrito = 1, Fecha = "17/09/2015", UsuarioId = 1 },
                 new Carrito() { IdCarrito = 2, Fecha = "17/09/2015", UsuarioId = 2 }
             );

            context.Productoes.AddOrUpdate(p => p.IdProducto,
            new Producto() { IdProducto = 1, NombreProducto = "PC AREA 51 ", Descripcion = "Marca: Alienware", Foto = "pcalien.png", Precio = 18000 },
            new Producto() { IdProducto = 2, NombreProducto = "MAC PRO ", Descripcion = "Marca: Apple", Foto = "macpro.png", Precio = 15000 }
            );

        }
    }
}
