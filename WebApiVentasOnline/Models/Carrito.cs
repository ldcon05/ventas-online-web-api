﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApiVentasOnline.Models
{
    public class Carrito
    {

        [Key]
        public int IdCarrito { set; get; }

        [Required]
        public string Fecha { set; get; }

        //Foreign Key

        public int UsuarioId { set; get; }

        public Usuario Usuario { set; get; }

    }
}