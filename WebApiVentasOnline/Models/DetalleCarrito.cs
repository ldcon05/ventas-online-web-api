﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApiVentasOnline.Models
{
    public class DetalleCarrito
    {
        [Key]
        public int IdDetalle { set; get; }

        [Required]
        public int Cantidad { set; get; }

        //Foreing key
        public int CarritoId { set; get; }
        public int ProductoId { set; get; }

        public Carrito Carrito { set; get; }
        public Producto Producto { set; get; }

    }
}