﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApiVentasOnline.Models
{
    public class Factura
    {

        [Key]
        public int IdFactura { set; get; }

        //Foreign Key
        public int CarritoId { set; get; }

        public Carrito Carrito { set; get; }

    }
}