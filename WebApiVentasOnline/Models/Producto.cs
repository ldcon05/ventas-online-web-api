﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApiVentasOnline.Models
{
    public class Producto
    {

        [Key] 
        public int IdProducto { get; set; }

        [Required]
        public String NombreProducto { get; set; }

        [Required]
        public String Descripcion { get; set; }

        [Required]
        public decimal Precio { get; set; }

        [Required]
        public String Foto { get; set; }

    }
}