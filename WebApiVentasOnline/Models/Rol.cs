﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApiVentasOnline.Models
{
    public class Rol
    {
        [Key]
        public int IdRol { get; set; }
        
        [Required]
        public String NombreRol { get; set; }

    }
}