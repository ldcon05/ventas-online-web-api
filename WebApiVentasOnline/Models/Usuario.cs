﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApiVentasOnline.Models
{
    public class Usuario
    {
         [Key]
        public int IdUsuario { get; set; }

         [Required]
         public String Nombre { get; set; }

         [Required]
         public String Apellido { get; set; }

         [Required]
         public String Nickname { get; set; }

         [Required]
         public String Pass { get; set; }

         public int Telefono { get; set; }

         [Required]
         public String Direccion { get; set; }

         public String Correo { get; set; }

         //Foreign Key
         public int RolId { get; set; }

         public Rol Rol { get; set; }


    }
}