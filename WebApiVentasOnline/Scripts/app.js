﻿var viewModel = function () {
    var self = this;
    //Declaracion de Variables
    self.productos = ko.observableArray();
    self.carritos = ko.observableArray();
    self.productosCarrito = ko.observableArray();
    self.usuarios = ko.observableArray();
    self.detalleCarritos = ko.observableArray();

    self.detalleProducto = ko.observable();
    self.productoCarrito = ko.observable();
    self.error = ko.observable();
    self.carritoEnUso = ko.observable();
    self.editarProducto = ko.observable();
    self.usuarioLogeado = ko.observable();
    self.operacionCantidad = ko.observable();

    var us;

    // Plantilla agregar y Modificar Producto
    self.nuevoProducto = {
        NombreProducto: ko.observable(),
        Descripcion: ko.observable(),
        Precio: ko.observable(),
        Foto: ko.observable()
    }

    self.editarProductoSeleccionado = {
        NombreProducto:"",
        Descripcion: "",
        Precio: "",
        Foto: ""
    }

    // Plantilla Usuarios
    
    self.nuevoUsuario = {
        Nombre: ko.observable(),
        Apellido: ko.observable(),
        Nickname: ko.observable(),
        Pass: ko.observable(),
        Telefono: ko.observable(),
        Dirreccion: ko.observable(),
        Correo: ko.observable(),
        RolId: ""
    }

    //Usuario Logear

    self.usuarioLogear = {
        NickName: ko.observable(),
        Password: ko.observable()
    }

    //Decalracion de variables URI

    var usuariosUri = '/api/Usuario/';
    var productosUri = '/api/Producto/';
    var carritoUri = '/api/Carrito/';
    var detalleCarritoUri = '/api/DetalleCarrito/';
    var faturaUri = '/api/Factura/';
    var carritoVentaUri = '/CarritoVenta/';


    // Ajax helper
    function AjaxHelper(uri, method, data) {
        self.error(''); // Clear error message
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }


    //Producto


    //Metodo que agregar los productos
    self.agregarProducto = function (formElement) {

        var producto = {
            NombreProducto: self.nuevoProducto.NombreProducto(),
            Descripcion: self.nuevoProducto.Descripcion(),
            Foto: self.nuevoProducto.Foto(),
            Precio: self.nuevoProducto.Precio()
        }

        AjaxHelper(productosUri, 'POST', producto).done(function (item) {
            self.productos.push(item);
        });

    }


    //  Metodo que obtine el detalle del producto
    self.getDetalleProducto = function (item) {
        AjaxHelper(productosUri + item.IdProducto, 'GET').done(function (data) {
            self.detalleProducto(data);
        });
    }
    //  Metodo que elimina el producto
    self.getEliminarProducto = function (item) {
        AjaxHelper(productosUri + item.IdProducto, 'DELETE').done(function (data) {
            self.productos.remove(item)
            self.detalleProducto(null);
        });
    } 

    //  Metodo que agrega productos al producto

    self.agregarProductoCarrito = function (item) {

        for (var i = 0 ; i < self.carritos().length; i++) {
            if (self.carritos()[i].UsuarioId === self.usuarioLogeado().IdUsuario) {
                self.carritoEnUso(self.carritos()[i]);
            }
        }

        var carritoProducto = {
            Cantidad: 1,
            CarritoId: self.carritoEnUso().IdCarrito,
            Productoid: item.IdProducto
        }

        AjaxHelper(detalleCarritoUri, 'POST', carritoProducto).done(function (item) {
            self.detalleCarritos.push(item);
            location.assign('http://localhost:51484/CarritoVenta');
        });

    }
    //  Metodo que retorna el producto a editar el producto
    self.getEditarProducto = function (item) {
        AjaxHelper(productosUri + item.IdProducto, 'GET').done(function (data) {
            self.editarProducto(data);
        });
    }

    // Metodo que retorna todos los usuarios
    function getListaUsuarios() {
        AjaxHelper(usuariosUri, 'GET').done(function (data) {
            self.usuarios(data);
        });
    }
    // Metodo que retorna todos los productos
    function getListaProductos() {
        AjaxHelper(productosUri, 'GET').done(function (data) {
            self.productos(data);
        });
    }

    // Metodo que retorna todos los carritos
    function getListaCarritos() {
        AjaxHelper(carritoUri, 'GET').done(function (data) {
            self.carritos(data);
        });
    }

    // Metodo que retorna todos los Los detalles del carrito
    function getListaDetalleCarritos() {
        AjaxHelper(detalleCarritoUri, 'GET').done(function (data) {
            self.detalleCarritos(data);
            if (!self.carritos().length == 0) {
                getListaPRO()
            } else {
                getListaDetalleCarritos();
            }
        });

    }

    // Metodo que simula un Inner JOIN para el detalle
    function getListaPRO(item) {
        for (var i = 0 ; i < self.carritos().length; i++) {
            if (self.carritos()[i].UsuarioId == self.usuarioLogeado().IdUsuario) {
                self.carritoEnUso(self.carritos()[i]);
            }
        }

        ko.utils.arrayForEach(self.detalleCarritos(), function (item) {
            if (item.CarritoId.toString() == self.carritoEnUso().IdCarrito.toString()) {
                ko.utils.arrayForEach(self.productos(), function (pro) {
                    if (item.ProductoId == pro.IdProducto) {
                        var productoCarri = {
                            IdDetalle: item.IdDetalle,
                            Cantidad: item.Cantidad,
                            CarritoId: item.CarritoId,
                            ProductoId: item.ProductoId,
                            NombreProducto: pro.NombreProducto,
                            Descripcion: pro.Descripcion,
                            Precio: pro.Precio,
                            Foto: pro.Foto
                        }
                        self.productosCarrito.push(productoCarri);
                    }
                });
            }
        });
        if (self.productosCarrito().length == 0) {
            getListaDetalleCarritos();
        }
    }

    //Metodo para incrementar la cantidad de un producto en el carrito

    self.IncrementarCantidad = function (item) {
        AjaxHelper(detalleCarritoUri + item.IdDetalle, 'GET').done(function (data) {
            self.operacionCantidad(data);
            var cambiarCantidad = {
                IdDetalle: self.operacionCantidad().IdDetalle,
                Cantidad: self.operacionCantidad().Cantidad + 1,
                CarritoId: self.operacionCantidad().CarritoId,
                ProductoId: self.operacionCantidad().ProductoId,
                Carrito: self.operacionCantidad().Carrito,
                Producto: self.operacionCantidad().Producto
            }

            AjaxHelper(detalleCarritoUri + self.operacionCantidad().IdDetalle, 'PUT', cambiarCantidad).done(function (data) {
                self.detalleCarritos.replace(item, data);
                location.reload();
                self.operacionCantidad(null);
            });

        });
    }

    //Metodo para decrementar la cantidad de un producto en el carrito
    self.DecrementarCantidad = function (item) {
        AjaxHelper(detalleCarritoUri + item.IdDetalle, 'GET').done(function (data) {
            self.operacionCantidad(data);
            if (self.operacionCantidad().Cantidad > 1) {
                var cambiarCantidad = {
                    IdDetalle: self.operacionCantidad().IdDetalle,
                    Cantidad: self.operacionCantidad().Cantidad - 1,
                    CarritoId: self.operacionCantidad().CarritoId,
                    ProductoId: self.operacionCantidad().ProductoId,
                    Carrito: self.operacionCantidad().Carrito,
                    Producto: self.operacionCantidad().Producto
                }
                
                AjaxHelper(detalleCarritoUri + self.operacionCantidad().IdDetalle, 'PUT', cambiarCantidad).done(function (data) {
                    self.detalleCarritos.replace(item, data);
                    self.operacionCantidad(null);
                    location.reload();
                });
            } else {
                alert('La cantidad de producto no puede ser menor a 1');
            }
        });
    }

    //Metodo que eiminana un producto del carrito
    self.getEliminarProductoCarrito = function (item) {
        AjaxHelper(detalleCarritoUri + item.IdDetalle, 'DELETE').done(function (data) {
            self.detalleCarritos.remove(item);
            location.reload();
        });
    }

    //Metodo que Registrar un Usuario
    self.agregarUsuario = function (formElement) {

        var rolId;
        alert(self.usuarioLogeado().RolId);
        if (self.usuarioLogeado().RolId == 1 || self.usuarioLogeado().RolId == 2) {
            rolId = self.usuarioLogeado().RolId;
        }else {
            rolId= 2;
        }

        var registro = {
            Nombre: self.nuevoUsuario.Nombre(),
            Apellido: self.nuevoUsuario.Apellido(),
            Nickname: self.nuevoUsuario.Nickname(),
            Pass: self.nuevoUsuario.Pass(),
            Telefono: self.nuevoUsuario.Telefono(),
            Direccion: self.nuevoUsuario.Dirreccion(),
            Correo: self.nuevoUsuario.Correo(),
            RolId: rolId
        }

        AjaxHelper(usuariosUri, 'POST', registro).done(function (data) {
            self.usuarios.push(data);
        });

    }
    


    // Metodo que carga el usuario logeado desde la cookie 
    function cargarUsuario() {
        var parsed = JSON.parse(leerCookie('usuario'));
        if (parsed) {
            self.usuarioLogeado(parsed);
            getListaCarritos();
            getListaDetalleCarritos();
        }
    }

    //  Metodo que edita el producto
    self.productoEditar = function (formElement) {
        var productoEditarSeleccionado = {
            IdProducto: self.editarProducto().IdProducto,
            NombreProducto: self.editarProductoSeleccionado.NombreProducto == "" ? self.editarProducto().NombreProducto : self.editarProductoSeleccionado.NombreProducto,
            Descripcion: self.editarProductoSeleccionado.Descripcion == "" ? self.editarProducto().Descripcion : self.editarProductoSeleccionado.Descripcion,
            Precio: self.editarProductoSeleccionado.Precio == "" ? self.editarProducto().Precio : self.editarProductoSeleccionado.Precio,
            Foto: self.editarProductoSeleccionado.Foto == "" ? self.editarProducto().Foto : self.editarProductoSeleccionado.Foto
           
        }
        AjaxHelper(productosUri + self.editarProducto().IdProducto, 'PUT', productoEditarSeleccionado).done(function (data) {
            self.productos.replace(self.editarProducto(), data);
            self.editarProducto(null);
            getListaProductos();
        });
    }

    // Metodo Logear
    var user;
    self.Logear = function (formElement) {
        var usuarioL = {
            NickName: self.usuarioLogear.NickName(),
            Password: self.usuarioLogear.Password()
        }
        verificar = false;
        for (var i = 0 ; i < self.usuarios().length; i++) {
            if (self.usuarios()[i].Nickname == usuarioL.NickName & self.usuarios()[i].Pass == usuarioL.Password) {
                verificar = true;
                self.usuarioLogeado(self.usuarios()[i])
                user = {
                    IdUsuario: self.usuarioLogeado().IdUsuario,
                    RolId: self.usuarioLogeado().RolId,
                    Nombre: self.usuarioLogeado().Nombre,
                    Apellido: self.usuarioLogeado().Apellido,
                    Nickname: self.usuarioLogeado().Nickname,
                    Pass: self.usuarioLogeado().Pass,
                    Telefono: self.usuarioLogeado().Telefono,
                    Dirreccion: self.usuarioLogeado().Dirreccion,
                    Correo: self.usuarioLogeado().Correo
                }
                crearCookie('usuario', JSON.stringify(user));
                var parsed = JSON.parse(leerCookie('usuario'));
                self.usuarioLogeado(parsed);
            }
        }
        if (verificar) {
            document.location.assign('/');
        } else {
            alert('Verifique sus credenciales');
        }

    }

    // Metodo que crea la sesion
    self.CerrarSesion = function (item) {
        eliminarCookie('usuario');
        location.assign('/');
    }

    //Cookies

    var crearCookie = function (key, value) {
        expires = new Date();
        expires.setTime(expires.getTime() + 31536000000); // Estableces el tiempo de expiración, genius
        cookie = key + "=" + value + ";expires=" + expires.toUTCString();
        return document.cookie = cookie;
    }

    var leerCookie = function (key) {
        keyValue = document.cookie.match("(^|;) ?" + key + "=([^;]*)(;|$)");
        if (keyValue) {
            return keyValue[2];
        } else {
            return null;
        }
    }

    var eliminarCookie = function (llave) {
        return document.cookie = llave + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }

    cargarUsuario();
    getListaProductos();
    getListaUsuarios();
};

ko.applyBindings(new viewModel());